<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Support\Facades\Hash;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

Route::group(
    ['prefix' => LaravelLocalization::setLocale(),
     'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ],
    function()
    {
        Route::get('/', function()
        {
            return view('welcome');
        });
        Route::prefix('/admin')->attribute('namespace', 'Admin')->group(function(){
            Route::get('/', 'HomeController@index')->name('admin.home.index');
            Route::resource('products', 'ProductController', ['as' => 'admin']);    
            Route::resource('orders', 'OrdersController', ['as' => 'admin']);    
            Route::resource('branches', 'BranchesController', ['as' => 'admin']);    
            Route::resource('users', 'UserController', ['as' => 'admin']);    
            Route::resource('payments', 'PaymentsController', ['as' => 'admin']);    
        });
        Route::get('/test' , function(){
            $credentials  = ['phone'=>'+1-386-764-6166','password'=>'123456789'];
           // dd(auth('api'));
            $user = auth('api')->attempt($credentials);
            dd($user);
        });
        
        

});

