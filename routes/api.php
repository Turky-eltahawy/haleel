<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', 'AuthController@login');
Route::post('/register', 'AuthController@register');
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/user', 'AuthController@me');
    Route::get('/refresh', 'AuthController@refresh');
    Route::post('/logout', 'AuthController@logout');
});

Route::get('nearest-branches', 'BranchController@nearest');
