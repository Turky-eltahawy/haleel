<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Laravel</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

        </style>
        {{-- login page  --}}
            <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>       
            <link rel="stylesheet" type="text/css" href={{asset("assets/vendor/bootstrap/css/bootstrap.min.css")}}> 
            <link rel="stylesheet" type="text/css" href={{asset("assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css")}}>
            <link rel="stylesheet" type="text/css" href={{asset("assets/fonts/iconic/css/material-design-iconic-font.min.css")}}>
            <link rel="stylesheet" type="text/css" href={{asset("assets/vendor/animate/animate.css")}}>
            <link rel="stylesheet" type="text/css" href={{asset("assets/vendor/css-hamburgers/hamburgers.min.css")}}>
            <link rel="stylesheet" type="text/css" href={{asset("assets/vendor/animsition/css/animsition.min.css")}}>
            <link rel="stylesheet" type="text/css" href={{asset("assets/vendor/select2/select2.min.css")}}>
            <link rel="stylesheet" type="text/css" href={{asset("assets/vendor/daterangepicker/daterangepicker.css")}}>
            <link rel="stylesheet" type="text/css" href={{asset("assets/css/util.css")}}>
            <link rel="stylesheet" type="text/css" href={{asset("assets/css/main1.css")}}>
        {{--  --}}
    </head>
    <body>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <form class="login100-form validate-form">
                        <span class="login100-form-title p-b-26">
                            Halel
                        </span>
                        <span class="login100-form-title p-b-48">
                            <i class="zmdi zmdi-font"></i>
                        </span>
    
                        <div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
                            <input class="input100" type="text" name="email">
                            <span class="focus-input100" data-placeholder="Email"></span>
                        </div>
    
                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <span class="btn-show-pass">
                                <i class="zmdi zmdi-eye"></i>
                            </span>
                            <input class="input100" type="password" name="pass">
                            <span class="focus-input100" data-placeholder="Password"></span>
                        </div>
    
                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn">
                                    Login
                                </button>
                            </div>
                        </div>
    
                        {{-- <div class="text-center p-t-115">
                            <span class="txt1">
                            </span>
    
                            <a class="txt2" href="#">
                            </a>
                        </div> --}}
                    </form>
                </div>
            </div>
        </div>
        <div id="dropDownSelect1"></div>
    	<script src={{asset("assets/vendor/jquery/jquery-3.2.1.min.js")}}></script>
<!--===============================================================================================-->
	<script src={{asset("assets/vendor/animsition/js/animsition.min.js")}}></script>
<!--===============================================================================================-->
	<script src={{asset("assets/vendor/bootstrap/js/popper.js")}}></script>
	<script src={{asset("assets/vendor/bootstrap/js/bootstrap.min.js")}}></script>
<!--===============================================================================================-->
	<script src={{asset("assets/vendor/select2/select2.min.js")}}></script>
<!--===============================================================================================-->
	<script src={{asset("assets/vendor/daterangepicker/moment.min.js")}}></script>
	<script src={{asset("assets/vendor/daterangepicker/daterangepicker.js")}}></script>
<!--===============================================================================================-->
	<script src={{asset("assets/vendor/countdowntime/countdowntime.js")}}></script>
<!--===============================================================================================-->
	<script src={{asset("assets/js/main1.js")}}></script>
    </body>
</html>
