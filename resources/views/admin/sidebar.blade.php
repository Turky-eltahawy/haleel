<aside class="app-sidebar">
    <div class="app-sidebar__user">
        <div class="dropdown user-pro-body text-center">
            <div class="nav-link pl-1 pr-1 leading-none ">
                {{-- <img src="{{ asset( auth()->user()->image) }}" alt="user-img" --}}
                     {{-- class="avatar-xl rounded-circle mb-1"> --}}
                <span class="pulse bg-success" aria-hidden="true"></span>
            </div>
            <div class="user-info">
                {{-- <h6 class=" mb-1 text-dark">{{auth()->user()->user_name}}</h6> --}}
                {{-- <span class="text-muted app-sidebar__user-nam e text-sm">{{ auth()->user()->email }} </span> --}}
            </div>
        </div>
    </div>
    <ul class="side-menu">
        <li>
            <a class="slide-item" href="#">
                <span><i class="side-menu__icon fa fa-area-chart"></i>{{ trans('dashboard') }}</span>

            </a>
        </li>
        
        <li>
            <a class="slide-item" href="{{ route('admin.branches.index') }}">
                <span><i class="side-menu__icon fa fa-shopping-cart"></i>{{ trans('branches') }}</span>
            </a>
        </li>
        <li>
            <a class="slide-item" href="{{ route('admin.products.index') }}">
                <span><i class="side-menu__icon fa fa-shopping-cart"></i>{{ trans('products') }}</span>
            </a>
        </li>

        <li>
            <a class="slide-item" href="{{ route('admin.orders.index') }}">
                <span><i class="side-menu__icon fa fa fa-money"></i>{{ trans('orders') }}</span>
            </a>
        </li>

        <li>
            <a class="slide-item" href="{{ route('admin.payments.index') }}">
                <span><i class="side-menu__icon fa fa fa-money"></i>{{ trans('payments') }}</span>
            </a>
        </li>
        <li>
            <a class="slide-item" href="{{ route('admin.users.index') }}">
                <span><i class="side-menu__icon fa fa-user"></i>{{ trans('users') }}</span>
            </a>
        </li>
    </ul>
</aside>
