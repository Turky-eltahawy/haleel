@extends('admin.layout')
@section('content')
<?php
use \App\Constants\ClassTypes;
?>
    <div class="app-content">
        <section class="section">

            <!--page-header open-->
            <div class="page-header">
                <h4 class="page-title">locations</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}" class="text-light-color">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('admin.locations.index') }}" class="text-light-color">locations</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Update locations #{{ $item->id }}</li>
                </ol>
            </div>
            <!--page-header closed-->

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>New location</h4>
                            </div>
                            <div class="card-body">
                                @include('admin.errors')
                                <form action="{{ route('admin.locations.update', ['location' => $item]) }}" method="post">
                                    @method("PUT")
                                    @csrf
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <li class="nav-item">
                                                <a class="nav-link {{ $lang == app()->getLocale() ? 'active': ''}} show" id="home-tab2" data-toggle="tab" href="#lang-{{ $lang }}" role="tab" aria-controls="home" aria-selected="true">{{ $language }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <div class="tab-pane fade {{ $lang == app()->getLocale() ? 'active show': ''}}" id="lang-{{ $lang }}" role="tabpanel" aria-labelledby="home-tab2">
                                                <div class="form-group col-md-4">
                                                     <label for="name">Name</label>
                                                     <input type="text" class="form-control" name="{{ $lang }}[name]" value="{{ $item->translate($lang)->name }}" id="{{ $lang }}[name]">
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="form-group col-md-4">
                                             <label for="Acronym">longitude</label>
                                             <input type="text" class="form-control" name="long" value="{{ $item->long }}" id="long" >
                                        </div>
                                        <div class="form-group col-md-4">
                                             <label for="Acronym">latitude</label>
                                             <input type="text" class="form-control" name="lat" value="{{ $item->lat }}" id="lat" >
                                        </div>

                                    </div>
                                    <div class="form-group col-md-3">
                                        <div class="form-group overflow-hidden">
                                            <label for="class">{{trans('class')}}</label>
                                            <select name="class"  class="form-control select2 w-100">
                                                <option value="">{{ trans('select_class') }}</option>
                                                @foreach(ClassTypes::getList() as $key => $value)
                                                    <option value="{{ $key }}" {{ $item->class == $key ? "selected":null }}> {{ $value }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="active" value="1" {{ $item->active ? 'checked' : '' }} class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">Active</span>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5"><i class="fa fa-save"></i> Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
