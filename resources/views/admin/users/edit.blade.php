@extends('admin.layout')

@section('content')
<?php

use \App\Constants\UserTypes;
?>
<div class="app-content">
    <section class="section">

        <!--page-header open-->
        <div class="page-header">
            <h4 class="page-title">{{trans('users')}}</h4>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.home.index') }}" class="text-light-color">{{trans('home')}}</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}" class="text-light-color">{{trans('users')}}</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{trans('update_user')}}</li>
            </ol>
        </div>
        <!--page-header closed-->

        <div class="section-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>{{trans('new_user')}}</h4>
                        </div>
                        <div class="card-body">
                            @include('admin.errors')
                            <form action="{{ route('admin.users.update', ['user' => $user]) }}" method="Post" enctype="multipart/form-data" autocomplete="off" >
                                @method('PUT')
                                @csrf
                                <input type="hidden" name="id" value="{{ $user->id}}" />


                                <div class="form-group col-md-12 row">
                                    <div class="form-group col-md-6">
                                        <label for="first_name">{{trans('first_name')}}</label>
                                        <input type="text" class="form-control" name="first_name" id="first_name" value="{{ !(old('first_name'))? $user->first_name : old('first_name' )}}" >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="last_name">{{trans('last_name')}}</label>
                                        <input type="text" class="form-control" name="last_name" id="last_name" value="{{ !(old('last_name'))? $user->last_name : old('last_name') }}" >
                                    </div>
                                </div>

                                <div class="form-group col-md-12 row">

                                    <div class="form-group col-md-6">
                                        <label for="email">{{trans('email')}}</label>
                                        <input type="email" class="form-control" name="email" id="email"  value="{{ !(old('email'))? $user->email : old('email') }}" >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone">{{trans('phone')}}</label>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <input type="text" class="form-control"  name="phone" id="phone"  value="{{ !(old('phone'))? $user->phone : old('phone') }}" data-inputmask='"mask": "99999999999"' data-mask >
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 row">

                                    <div class="form-group col-md-6">
                                        <label for="password">{{trans('password')}}</label>
                                        <input type="password" class="form-control" name="password" id="password"  >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label for="password_confirmation">{{trans('password_confirmation')}}</label>
                                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" >
                                    </div>
                                </div>
                                <div class="form-group col-md-12 row">
                                    <div class="form-group col-md-6">
                                        <div class="form-group overflow-hidden">
                                            <label for="types">{{trans('user_type')}}</label>
                                            <select name="type"  class="form-control select2 w-100" id="types" >
                                                <option value="">{{ trans('select_user_type') }}</option>
                                                @foreach(UserTypes::getList() as $key => $value)
                                                <option value="{{ $key }}" {{ $user->type == $key ? "selected":null }}> {{ $value }} </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                
                                    <div class="form-group col-md-6" style="margin-top:25px;">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="active" value="1" {{ $user->active ? 'checked' : '' }} class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">{{trans('active')}}</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group col-md-12 row">
                                    <div class="col-md-3"></div>
                                    
                                    <div class="form-group col-md-6">
                                        <label class="custom-switch"> {{ trans('choose_profile_picture') }}
                                            <input type="file"  style="display:none;" onchange="readURL(this,'upload_img');" name="image" class="image" >
                                            <img id="upload_img" src="{{ asset( $user->image )}}">
                                        </label>
                                    </div>
                                </div>
                                    <div class="col-md-3"></div>
                                </div>

                                
                                
                                <div class="form-group col-md-12 row">
                                    <div class="form-group col-md-3">
                                        <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5"><i class="fa fa-save"></i> {{trans('save')}}</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </section>
</div>
@stop
@section('scripts')
<script>


</script>
@stop
