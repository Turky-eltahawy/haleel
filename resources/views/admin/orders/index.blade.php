@extends('admin.layout')

@section('content')
<?php
use \App\Constants\OrderStatus;
?>
    <div class="app-content">
        <section class="section">

            <!--page-header open-->
            <div class="page-header">
                <h4 class="page-title">{{trans('orders')}}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="/" class="text-light-color">{{trans('home')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{trans('orders')}}</li>
                </ol>
            </div>
            <!--page-header closed-->
            <div class="row">
                <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="form-horizontal" type="get" action="{{ route("admin.orders.index") }}">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <select name="status"  class="form-control select2 w-100" id="status" >
                                            <option value="" >{{ trans('select_status') }}</option>
                                            @foreach($status as $key =>$value)
                                                <option value="{{ $key }}" {{ request("status") == $key ? "selected":null }}>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                          
                                    <div class="col-md-6">
                                        <select name="payment_type"  class="form-control select2 w-100" id="payment_type" >
                                            <option value="" >{{ trans('select_payment_type') }}</option>
                                            @foreach($paymentTypes as $key =>$value)
                                                <option value="{{ $key }}" {{ request("payment_type") == $key ? "selected":null }}>{{ $value }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6 input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" placeholder="{{ trans('from_date') }}" class="form-control dateFrom" value="{{ request("from_date") }}" name="from_date" readonly >
                                    </div>

                                    <div class="col-md-6 input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" placeholder="{{ trans('to_date') }}" class="form-control dateTo" value="{{ request("to_date") }}" name="to_date" readonly >
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <select name="banch_id"  class="form-control select2 w-100" id="banch_id" >
                                            <option value="" >{{ trans('select_banch') }}</option>
                                            @foreach($branchs as $branch_id )
                                                <option value="{{ $branch_id }}" {{ request("banch_id") == $branch_id ? "selected":null }}>{{ $branch->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary  mt-1 mb-0">{{ trans("search") }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <span class="table-add float-right">
                                    <a href="{{ route('admin.orders.create') }}" class="btn btn-icon"><i class="fa fa-plus fa-1x" aria-hidden="true"></i></a>
                                </span>
                                <h4>
                                    {{trans('orders_list')}}
                                    (<small>{{ trans('num_rows') }}</small> <span class="badge badge-success">{{ $ordersCount }}</span>)
                                </h4>

                            </div>

                            <div class="card-body">
                                @if(session()->has('success'))
                                    <div class="alert alert-success alert-has-icon alert-dismissible show fade">
                                        <div class="alert-icon"><i class="ion ion-ios-lightbulb-outline"></i></div>
                                        <div class="alert-body">
                                            <button class="close" data-dismiss="alert">
                                                <span>×</span>
                                            </button>
                                            <div class="alert-title">{{trans('success')}}</div>
                                            {{ session('success') }}
                                        </div>
                                    </div>
                                @endif

                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover mb-0 text-nowrap">
                                        <thead>
                                            <tr>
                                                <th style="width: 1px">#</th>
                                                <th>{{trans('client_name')}}</th>
                                                <th>{{trans('total_cost')}}</th>
                                                <th>{{trans('address')}}</th>
                                                <th>{{trans('status')}}</th>
                                                <th style="width: 1px">{{trans('actions')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($list as $order)
                                            <tr>
                                                <td>{{ $order->id }}</td>
                                                <td>{{ $order->user ? $order->user->name : '-' }}</td>
                                                <td>{{ $order->total_price ? $order->total_price  : '-'}}</td>
                                                <td>{{ $order->address->location->name }}</td>
                                                <td>
                                                @foreach(OrderStatus::getList() as $key => $value)

                                                    @if($order->status == $key)
                                                        {{$value}}
                                                    @endif

                                                @endforeach
                                                </td>

                                                <td>
                                                    <div class="btn-group dropdown">
                                                        <button type="button" class="btn btn-sm btn-info m-b-5 m-t-5 dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa-cog fa"></i>
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            @can('update', $order)
                                                                <a class="dropdown-item has-icon" href="{{ route('admin.orders.edit', ['order' => $order->id]) }}"><i class="fa fa-edit"></i> {{trans('edit')}}</a>
                                                            @endcan
                                                            <a class="dropdown-item has-icon" href="{{ route('admin.order.products.index', ['order' => $order->id]) }}"><i class="fa fa-shopping-cart"></i> {{trans('products')}}</a>
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="d-flex justify-content-center">
                                    {{ $list->links() }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
