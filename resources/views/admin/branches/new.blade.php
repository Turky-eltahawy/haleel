@extends('admin.layout')
@section('content')
<?php
use App\Constants\DayHours;
?>
    <div class="app-content">
        <section class="section">

            <!--page-header open-->
            <div class="page-header">
                <h4 class="page-title">{{trans('branches')}}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">{{trans('new')}}</li>
                </ol>
            </div>
            <!--page-header closed-->

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{trans('new_branch')}}</h4>
                            </div>
                            <div class="card-body">
                                @include('admin.errors')

                                @section("open-form-tag")
                                    <form action="{{ route('admin.branches.store') }}" method="post">
                                @show

                                    @csrf
                                    
                                        <div class="row" >
                                            <div class="form-group col-md-6">
                                                <label for="ar_name">{{trans('ar_name')}}</label>
                                                <input type="text" class="form-control" name="ar[name]" id="ar[name]">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="en_name">{{trans('en_name')}}</label>
                                                <input type="text" class="form-control" name="en[name]" id="en[name]">
                                            </div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="start_working_hours">{{trans('start_working_hours')}}</label>
                                            <select name="start_working_hours" id="start_working_hours" class="form-control">
                                                @foreach(DayHours::getList('AM') as $hour)
                                                    <option value="{{$hour}}"{{ old('start_working_hours') == $hour ? 'selected' :'' }}>{{$hour}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="end_working_hours">{{trans('end_working_hours')}}</label>
                                            <select name="end_working_hours" id="end_working_hours" class="form-control">
                                                @foreach(DayHours::getList('PM') as $hour)
                                                    <option value="{{$hour}}"{{ old('end_working_hours') == $hour ? 'selected' :'' }}>{{$hour}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class=" row">
                                        <div class="form-group col-md-6">
                                            <label for="name">{{trans('long')}}</label>
                                            <input type="text" value="{{old('long')}}" class="form-control" name="long"  id="long" placeholder="longitude"  >
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="name">{{trans('lat')}}</label>
                                            <input type="text" value="{{old('lat')}}"  class="form-control" name="lat"  id="lat" placeholder="latitude" >    
                                        </div>   
                                    </div>
                                 
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="name">{{trans('address')}}</label>
                                            <input type="text" class="form-control"  readonly name="address" value="{{old('address')}}" id ="address" >
                                        </div>
                                        <div class="form-group col-md-2" style="margin-top:25px;">
                                            <label class="custom-switch">
                                                <input type="checkbox" name="active" value="1" checked class="custom-switch-input">
                                                <span class="custom-switch-indicator"></span>
                                                <span class="custom-switch-description">{{trans('active')}}</span>
                                            </label>
                                        </div>
                                        @if ($errors->has('address'))
                                            <div class="col-md-4" style="margin-top:25px;">
                                                <span style="color:red;">{{trans('translations.drag')}}</span>
                                            </div>
                                        @endif
                                    </div>
                                    
                                    <div class="row">
                                        <div class="col-md-4"> 
                                            <label class="custom-switch"> {{ trans('branch_image') }}
                                              <input type="file"  style="display:none;" onchange="readURL(this,'upload_img');" name="image" class="image" >
                                              <img id="upload_img" src="{{ asset('assets/img/add_img.jpg')}}">
                                            </label>
                                        </div>

                                        <input id="pac-input" style="width:250px;height:25px;margin-top:20px;" class="controls" type="text" placeholder="{{trans('search')}}">
                                        <div class=" col-md-8" id="map" style="height: 400px; width: 70%"></div>
                                        <div class="col-md-2"></div>
                                    </div>
                                    

                                    
                                    <div class="form-group col-md-3">
                                        <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5"><i class="fa fa-save"></i> {{trans('save')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')
    <script>
        function readURL(input, img_id) {
    if (input.files && input.files[0]) {    
      var reader = new FileReader();
      var image_id=$('#' + img_id);
      reader.onload = function (e) {
          image_id.attr('src', e.target.result);
      };
      image_id.css("width", "260px");
      image_id.css("height", "261px");
      reader.readAsDataURL(input.files[0]);
    }

  }
  
        $(document).ready(function(){
            $('input.timepicker').timepicker({});
            $('.timepicker').timepicker({
                timeFormat: 'h:mm p',
                interval: 60,
                minTime: '10',
                maxTime: '6:00pm',
                defaultTime: '11',
                startTime: '10:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
        });
    </script>
@endsection
