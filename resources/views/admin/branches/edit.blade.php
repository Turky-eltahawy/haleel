@extends('admin.layout')
<?php use \App\Constants\UserTypes;?>
@section('content')
    <div class="app-content">
        <section class="section">

            <!--page-header open-->
            <div class="page-header">
                <h4 class="page-title">{{trans('branches')}}</h4>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a  href="{{ route('admin.home.index') }}" class="text-light-color">{{trans('home')}}</a></li>
                    <li class="breadcrumb-item"><a  href="{{ route('admin.branches.index') }}" class="text-light-color">{{trans('branches')}}</a></li>
                    <li class="breadcrumb-item active" aria-current="page">{{trans('update_branch')}} #{{ $branch->id }}</li>
                </ol>
            </div>
            <!--page-header closed-->

            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>{{trans('update_branch')}}</h4>
                            </div>
                            <div class="card-body">
                                @include('admin.errors')

                                @section("open-form-tag")
                                    <form action="{{ route('admin.branches.update', ['branch' => $branch]) }}" method="post">
                                @show

                                    @method("PATCH")
                                    @csrf
                                    <ul class="nav nav-tabs" role="tablist">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <li class="nav-item">
                                                <a class="nav-link {{ $lang == app()->getLocale() ? 'active': ''}} show" id="home-tab2" data-toggle="tab" href="#lang-{{ $lang }}" role="tab" aria-controls="home" aria-selected="true">{{ trans($language) }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="tab-content">
                                        @foreach(config()->get('app.locales') as $lang => $language)
                                            <div class="tab-pane fade {{ $lang == app()->getLocale() ? 'active show': ''}}" id="lang-{{ $lang }}" role="tabpanel" aria-labelledby="home-tab2">
                                                <div class="form-group col-md-4">
                                                    <label for="name">{{trans('name')}}</label>
                                                    <input type="text" class="form-control" name="{{ $lang }}[name]" value="{{ old($lang.".name", $branch->translate($lang)->name)}}" id="{{ $lang }}[name]">
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="name">{{trans('address')}}</label>
                                                    <input type="text" class="form-control" name="{{ $lang }}[address]" value="{{ old($lang.'.address',$branch->translate($lang)->address) }}" id="{{ $lang }}[address]">
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div class="form-group col-md-4">
                                        <div class="form-group overflow-hidden">
                                            <label for="type">{{trans('type')}}</label>
                                            <select name="type"  class="form-control select2 w-100" id="type" >
                                                <option value="">{{ trans('select_type') }}</option>
                                                @foreach($types ?? '' as $key => $value)
                                                    <option value="{{ $key }}" {{ old("type",$branch->type) == $key ? "selected":null }}>{{ $value }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-2">
                                        <label for="name">{{trans('latitude')}}</label>
                                        <input type="text" class="form-control" name="lat" id="lat" value="{{ old("lat", $branch->lat) }}">
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="name">{{trans('longitude')}}</label>
                                        <input type="text" class="form-control" name="lng" id="lng" value="{{ old("lng",$branch->lng) }}">
                                    </div>
                                        <div class="form-group col-md-4">
                                        <label for="start_working_hours">{{trans('start_working_hours')}}</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o inputIcon"></i>
                                            </div>
                                            <input class="form-control timepicker" type="text" name="start_working_hours" value="{{ old('start_working_hours',$branch->start_working_hours) }}">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="end_working_hours">{{trans('end_working_hours')}}</label>
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o inputIcon"></i>
                                            </div>
                                            <input class="form-control timepicker" type="text" name="end_working_hours" value="{{ old('end_working_hours',$branch->end_working_hours) }}">
                                        </div>
                                    </div>

                                        <div class="form-group col-md-3">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="stock_enabled" value="1" {{ old("stock_enabled", $branch->stock_enabled) ? 'checked' : '' }} class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">{{trans('stock_enabled')}}</span>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="custom-switch">
                                            <input type="checkbox" name="active" value="1" {{ old("active", $branch->active) ? 'checked' : '' }} class="custom-switch-input">
                                            <span class="custom-switch-indicator"></span>
                                            <span class="custom-switch-description">{{trans('active')}}</span>
                                        </label>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <button type="submit" href="#" class="btn  btn-outline-primary m-b-5  m-t-5"><i class="fa fa-save"></i> {{trans('save')}}</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('scripts')
    <script>
        $(document).ready(function(){
            $('input.timepicker').timepicker({});
            $('.timepicker').timepicker({
                timeFormat: 'h:mm p',
                interval: 60,
                minTime: '10',
                maxTime: '6:00pm',
                defaultTime: '11',
                startTime: '10:00',
                dynamic: false,
                dropdown: true,
                scrollbar: true
            });
        });
    </script>
@endsection
