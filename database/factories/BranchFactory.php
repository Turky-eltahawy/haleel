<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Branch;
use Faker\Generator as Faker;
use Grimzy\LaravelMysqlSpatial\Types\Point;

$factory->define(Branch::class, function (Faker $faker) {
    return [
        'location' => new Point($faker->latitude(), $faker->longitude()),
        'address' => $faker->address
    ];
});
