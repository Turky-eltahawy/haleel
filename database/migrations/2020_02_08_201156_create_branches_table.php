<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->json('address');
            $table->boolean('active')->default(1);
            $table->point('location');
            $table->timestamps();
        });

        // Schema::create('branch_translations', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->integer('branch_id')->unsigned();
        //     $table->string('name');
        //     $table->string('locale')->index();
        //     $table->unique(['branch_id','locale']);
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
        Schema::dropIfExists('branches_translations');
    }
}
