<?php

namespace App\Http\Services;

use App\Models\User;
use App\Repositories\UserRepository;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Hash;
use App\Http\Services\ExportService;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Response;

class UserService
{


    private $uploaderService;
    protected $userRepository;


    public function __construct(UploaderService $uploaderService, UserRepository $userRepository)
    {
        $this->uploaderService = $uploaderService;
        $this->userRepository = $userRepository;
    }

    public function fillFromRequest(Request $request, $user = null)
    {
        if (!$user) {
            $user = new User();
        }
        $user->fill($request->all());

        if ($request->hasFile('image')) {
            $user->image = $this->uploaderService->upload($request->file('image'), 'users');
        }
        $user->active = $request->input("active", 0);

        $user->save();
        return $user;
    }

    public function updateProfile(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        if (!$user) {
            throw new Exception('Exception message', Response::HTTP_UNAUTHORIZED);
        }

        if (!Hash::check($request->input('password'), $user->password)) {
            throw new Exception(trans('please_enter_correct_password'), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user->fill($request->only(['first_name', 'last_name', 'phone']));
        if ($request->hasFile('image')) {
            $this->uploaderService->deleteFile($user->image);
            $user->image = $this->uploaderService->upload($request->file('image'), 'users');
        }

        $user->save();

        return true;
    }

    /**
     * @param Request $request
     * @param null $user
     * @return bool
     */
    public function fillChangePasswordFromRequest(Request $request, $user = null)
    {
        if (!$user) {
            return false;
        }
        $currentPassword = $request->input('current_password');
        if (!Hash::check($currentPassword, $user->password)) {
            return false;
        }
        $user->fill($request->all());
        $user->save();
        return true;
    }

    /**
     * @param Request $request
     * @param null $user
     * @return bool|null
     */
    public function fillUserGroupsFromRequest(Request $request, $user = null)
    {
        if (!$user) {
            return false;
        }
        $user->groups()->sync($request->input("groups"));
        return $user;
    }

    /**
     * @param Request $request
     * @param null $address
     *
     * @return Address|null
     */
    public function fillUserAddress(Request $request, $address = null)
    {
        if (!$address) {
            $address = new address();
        }

        $address->fill($request->request->all());

        $address->save();
        return $address;
    }

    public function fillUserDeviceFromRequest(Request $request, $userDevice = null)
    {
        if (!$userDevice) {
            $userDevice = new UserDevice();
        }

        $userDevice->fill($request->request->all());
        $userDevice->save();

        return $userDevice;
    }

    public function fillUserPoints(Request $request, $point = null)
    {
        if (!$point) {
            $point = new Point();
        }

        $balance = $this->userRepository->getPointsBalance($request->input("user_id"));

        $point->fill($request->all());

        $point->balance = $balance + $request->input("amount");

        $point->save();

        return $point;
    }

    public function toggleFavouriteProduct(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $user->favoritedProducts()->toggle($request->request->get("branch_product_id"));
    }

    public function export()
    {
        $headings = [
            '#',
            'Name',
            'User Name',
            'Email',
            'Phone'
        ];
        $list = $this->userRepository->search(request())->get(['id','name','user_name','email','phone']);

        return Excel::download(new ExportService($list, $headings), 'Users Report.xlsx');
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function resetPhoneVerified()
    {
        $user = auth()->user();
        $user->phone_verify = code($numbers = 4, $type = 'digits');
        $user->save();
        return $user;
    }

    public function verifyUserPhone(Request $request)
    {
        $user = auth()->user();
        if ($user->phone_verify != $request->get("code")) {
            throw new Exception('Invalid Code', Response::HTTP_NOT_ACCEPTABLE);
        }
        $user->phone_verify = '';
        $user->active = true;
        $user->save();

        return true;
    }

    public function toggleAvailability(Request $request)
    {
        $user = auth()->user();
        $user->available = $request->get('available', 0);
        $user->save();

        return $user;
    }

    public function toggleNotification(Request $request)
    {
        $user = auth()->user();
        $user->notification = $request->get('notification', 0);
        $user->save();

        return $user;
    }

    public function contactUs(Request $request)
    {
        $contact = new Contact() ;
        $contact->email = $request->get('email');
        $contact->message = $request->get('message');
        $contact->save();
    }

    
}
