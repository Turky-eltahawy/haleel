<?php

namespace App\Http\Services;

use App\Models\Product;
use App\Models\ProductImage;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Resources\Products;

use File;

class ProductService
{

    /**
     * @var UploaderService
     */
    private $uploaderService;
    protected $productRepository;

    public function __construct(UploaderService $uploaderService, ProductRepository $productRepository)
    {
        $this->uploaderService = $uploaderService;
        $this->productRepository = $productRepository;
    }

    public function fillFromRequest(Request $request, $product = null)
    {
        if (!$product) {
            $product = new Product() ;
        }

        $product->fill($request->all());

        $product->active = $request->input("active", 0);

        $product->save() ;

        if ($request->has("images")) {
            $this->saveProductImages($request->file("images"), $product);
        }
        return $product ;
    }

    private function saveProductImages($images, Product $product)
    {

        $productImages = [] ;

        foreach ($images as $image) {
            $img = $this->uploaderService->upload($image, "products");
            $productImages[] = new ProductImage(["image" => $img]);
        }

        $product->images()->saveMany($productImages);
    }

    public function deleteImage(Product $product, ProductImage $productImage)
    {
        if (count($product->images) > 1) {
            $productImage->delete();
            return true ;
        }

        return false;
    }

}
