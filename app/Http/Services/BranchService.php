<?php

namespace App\Http\Services;

use App\Models\Branch;
use App\Repositories\BranchRepository;
use Symfony\Component\HttpFoundation\Request;


class BranchService
{
    protected $branchRepository;

    public function __construct(BranchRepository $branchRepository)
    {
        $this->branchRepository = $branchRepository;
    }

    public function fillFromRequest(Request $request, $branch = null)
    {
        if (!$branch) {
            $branch = new Branch();
        }
        // dd($request->all());
        $branch->fill($request->request->all());
        
        $branch->active = $request->request->get('active', 0);
        // $branch->start_working_hours = date("H:i:s", strtotime($request->input('start_working_hours')));
        // $branch->end_working_hours = date("H:i:s", strtotime($request->input('end_working_hours')));
        $branch->save();

        return $branch;
    }


}
