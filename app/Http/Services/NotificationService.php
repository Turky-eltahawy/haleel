<?php

namespace App\Http\Services;

use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Mail;

class NotificationService
{
    /**
     * @var SMSProviderInterface
     */
    private $SMSService;

    /**
     * NotificationService constructor.
     * @param SMSProviderInterface $SMSService
     */
    public function __construct(SMSProviderInterface $SMSService)
    {
        $this->SMSService = $SMSService;
    }

    /**
     * @param  Authenticatable  $user
     * @param  string  $text
     *
     * @return boolean
     */
    public function sendSMS(Authenticatable $user, string $text)
    {
        return $this->SMSService->sendSMS($user, $text);
    }

    public function sendMail($user, $mailable)
    {
        Mail::to($user->email)->send($mailable);
        return true;
    }

    public function sendPush($user)
    {
    }
}
