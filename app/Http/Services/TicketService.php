<?php

namespace App\Http\Services;

use App\Models\Ticket;
use App\Models\TicketReply;
use Symfony\Component\HttpFoundation\Request;
use Auth;
use App\Http\Services\ExportService;
use App\Http\Resources\Tickets;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\TicketRepository;


class TicketService
{

    protected $ticketRepository ;

    public function __construct(TicketRepository $ticketRepository ) {

        $this->ticketRepository = $ticketRepository;

    }

    public function fillFromRequest(Request $request, $Ticket = null)
    {

        if (!$Ticket) {
            $Ticket = new Ticket();
        }
        $Ticket->fill($request->request->all());

       if($request->has('ticketreason_id')){
            $Ticket->ticketReason_id = $request->request->get('ticketreason_id','0');
       }

        $Ticket->save();

        return $Ticket;
    }

    public function fillTicketReplyFromRequest(Request $request, $ticketReply = null)
    {
        if (!$ticketReply) {
            $ticketReply = new TicketReply();
        }
        $ticketReply->fill($request->request->all());
        $ticketReply->save();
        return $ticketReply;
    }

    public function export()
    {
        $headings = [
            '#',
            'user id',
            'Title',
            'Description',
            'Assigne id',
            'status',
            'created at'
        ];
        $list = $this->ticketRepository->search(request())->get();
        $listObjects = Tickets::collection($list);

        return Excel::download(new ExportService($listObjects, $headings), 'Tickets Report.xlsx');
    }
}
