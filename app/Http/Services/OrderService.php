<?php

namespace App\Http\Services;

use App\Models\Order;
use App\Models\OrderProduct;
use App\Http\Resources\Orders;
use Symfony\Component\HttpFoundation\Request;

class OrderService
{
    public function __construct()
    {
        
    }

    public function fillFromRequest(Request $request, $order = null)
    {
        if (!$order) {
            $order = new order();
        }

        $order->fill($request->request->all());

        $order->status = $request->get('status', OrderStatus::SUBMITTED);
        $order->save();
    
    }
    

}
