<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller ;
use App\Http\Requests\Admin\OrderProductsRequest;
use App\Http\Services\OrderService;
use App\Models\BranchProduct;
use App\Models\Category;
use App\Models\OrderProduct;
use App\Models\Order;
use App\Repositories\OrderProductRepository;
use View;

class OrderProductsController extends Controller
{
    protected $orderService;
    protected $orderProductRepository;

    public function __construct(OrderService $orderService, OrderProductRepository $orderProductRepository)
    {
        $this->orderService = $orderService;
        $this->orderProductRepository = $orderProductRepository;
    }

    public function index(Order $order)
    {
        $list = $order->products;
        return View::make('admin.orders.products.index', ['list' => $list,'order'=>$order]);
    }

    public function create(Order $order)
    {
        $branchProducts = BranchProduct::all();

        return View::make('admin.orders.products.new', ['order' => $order, 'branchProducts' => $branchProducts]);
    }


    public function store(OrderProductsRequest $request, Order $order)
    {
        $this->orderService->fillOrderProductsFromRequest($request);

        return redirect(route('admin.order.products.index', ['order' => $order->id]))->with('success', trans('order_product_added_successfully'));
    }

    public function edit(Order $order, OrderProduct $orderProduct)
    {
        $branchProducts = BranchProduct::all();

        return View::make('admin.orders.products.edit', ['orderProduct' => $orderProduct,'branchProducts'=>$branchProducts]);
    }

    public function update(OrderProductsRequest $request, Order $order, OrderProduct $orderProduct)
    {
        $this->orderService->fillOrderProductsFromRequest($request, $orderProduct);

        return redirect(route('admin.order.products.index', ['order' => $orderProduct->order_id]))->with('success', trans('order_product_updated_successfully'));
    }

    public function destroy(Order $order, OrderProduct $orderProduct)
    {
        $orderProduct->delete();

        return redirect()->back()->with('success', trans('order_product_deleted_successfully'));
    }

    public function consumption()
    {
        $categories = Category::all();
        $list = $this->orderProductRepository->consumption(request())->paginate(10);
        $list->appends(request()->all());

        return View::make('admin.consumption.index', ['list' => $list, 'categories'=>$categories]);
    }

    public function export()
    {
        return $this->orderProductRepository->export();
    }
}
