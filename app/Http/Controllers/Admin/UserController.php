<?php

namespace App\Http\Controllers\Admin;

use App\Constants\ClassTypes;
use App\Constants\UserTypes;
use App\Http\Requests\Admin\UserRequest;
use App\Http\Services\UploaderService;
use App\Http\Services\UserService;
use App\Models\User;
use App\Models\Vendor;
use App\Models\Company;
use App\Repositories\UserRepository;
use App\Repositories\BranchRepository;
use View;
use File;
use App\Http\Controllers\Controller ;

class UserController extends Controller
{
    protected $userService;
    public $userRepository;
    public $branchRepository;
    public $uploaderService;

    public function __construct(UserService $userService, UserRepository $userRepository
                               , UploaderService $uploaderService, BranchRepository $branchRepository )
    {
        // $this->authorizeResource(User::class, "user");
        $this->userService = $userService;
        $this->branchRepository = $branchRepository;
        $this->userRepository = $userRepository;
        $this->uploaderService = $uploaderService;
    }

    public function index()
    {
        // $this->authorize("index", User::class);
        $list = $this->userRepository->search(request())->paginate(10);
        $list->appends(request()->all());
        $count = $this->userRepository->search(request())->count();
        $types = UserTypes::getList();

        return View::make('admin.users.index', ['list' => $list, 'count' => $count, 'types' => $types]);
    }

    public function create()
    {
        $branchs = $this->branchRepository->search()->get();
        return View::make('admin.users.create', compact('branchs'));
    }

    public function store(UserRequest $request)
    {
        $this->userService->fillFromRequest($request);
        return redirect(route('admin.users.index'))->with('success', trans('item_added_successfully'));
    }

    public function edit(User $user)
    {
        return View::make('admin.users.edit', compact('user'));
    }

    public function update(UserRequest $request, User $user)
    {
        $this->userService->fillFromRequest($request, $user);
        return redirect(route('admin.users.index'))->with('success', trans('item_updated_successfully'));
    }

    public function destroy(User $user)
    {
        $this->uploaderService->deleteFile($user->image);
        $user->delete();

        return redirect()->back()->with('success', 'Item Deleted Successfully');
    }

}
