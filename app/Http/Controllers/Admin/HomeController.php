<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller ;
use App\Models\User;
use View;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {
        return View::make('admin.home.index');
    }

    private function buildTickets()
    {
        $tickets['submitted'] = $this->ticketRepository->ticketsGraph()->count();
        $tickets['pending'] = $this->ticketRepository->ticketsGraph(['status' => TicketStatus::PENDING])->count();
        $tickets['resolved'] = $this->ticketRepository->ticketsGraph(['status' => TicketStatus::RESOLVED])->count();

        return $tickets;
    }
}
