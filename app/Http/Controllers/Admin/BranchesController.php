<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\BranchRequest;
use App\Http\Controllers\Controller ;
use App\Models\Branch;
use App\Repositories\BranchRepository;
use App\Http\Services\BranchService;

class BranchesController extends Controller
{
    protected $branchService;
    protected $branchRepository;


    public function __construct(BranchService $branchService, BranchRepository $branchRepository)
    {
        // $this->authorizeResource(Branch::class, "branch");
        $this->branchService = $branchService;
        $this->branchRepository = $branchRepository;
    }

    public function index()
    {
        // $this->authorize("index", Branch::class);
        $list = $this->branchRepository->search(request())->paginate(10);
        $list->appends(request()->all());
        $count = $this->branchRepository->search(request())->count();

        return view('admin.branches.index', ['list' => $list, 'count' => $count]);
    }

    public function create()
    {
        return view('admin.branches.new');
    }

    public function store(BranchRequest $request)
    {
        $this->branchService->fillFromRequest($request);
        return redirect(route('admin.branches.index'))->with('success', trans('branch_added_successfully'));
    }

    public function destroy(Branch $branch)
    {
        $branch->delete();
        return redirect()->back()->with('success', trans('branch_deleted_successfully'));
    }

    public function edit(Branch $branch)
    {
        return view('admin.branches.edit', ['branch' => $branch]);
    }

    public function update(BranchRequest $request, Branch $branch)
    {
        $this->branchService->fillFromRequest($request, $branch);
        return redirect(route('admin.branches.index'))->with('success', trans('branch_updated_successfully'));
    }

    public function products(Branch $branch)
    {
        $list = $branch->products;
        return view('admin.branches.index', ['list' => $list,'branch'=>$branch]);
    }

}
