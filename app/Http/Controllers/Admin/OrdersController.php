<?php

namespace App\Http\Controllers\Admin;
use App\Repositories\BranchRepository ;
use App\Constants\OrderStatus;
use App\Constants\VendorTypes;
use App\Constants\PaymentTypes;
use App\Http\Controllers\Controller ;
use App\Http\Requests\Admin\OrderRequest;
use App\Http\Services\OrderService;
use App\Models\Order;
use App\Models\Branch;
use App\Models\User;
use App\Repositories\OrderRepository;
use View;
use Symfony\Component\HttpFoundation\Request;

class OrdersController extends Controller
{
    protected $orderService;
    protected $orderRepository;
    protected $branchRepository;

    public function __construct(OrderService $orderService, OrderRepository $orderRepository
                               , BranchRepository  $branchRepository
        )
    {
        $this->orderService = $orderService;
        $this->orderRepository = $orderRepository;
        $this->branchRepository = $branchRepository;
    }

    public function index()
    {
        $status = OrderStatus::getList();
        $paymentTypes = PaymentTypes::getList();
        $branchs = Branch::whereActive(1)->get();
        $ordersCount = $this->orderRepository->search()->count();
        $list = $this->orderRepository->search(request())->paginate(10);
        $list->appends(request()->all());

        return View::make('admin.orders.index', compact('branchs','list', 'status','ordersCount' , 'paymentTypes'));
    }

    public function create()
    {
        $branchs = Branch::all();

        return View::make('admin.orders.new', ['branchs'=>$branchs]);
    }

    public function store(OrderRequest $request)
    {
        $this->orderService->fillFromRequest($request);

        return redirect(route('admin.orders.index'))->with('success', trans('order_added_successfully'));
    }

    public function edit(Order $order)
    {
        $users = User::all();

        $addresses = Address::where('user_id', $order->user_id)->get();

        return View::make('admin.orders.edit', ['order' => $order,'users'=>$users, 'addresses'=>$addresses]);
    }

    public function update(OrderRequest $request, Order $order)
    {
        $this->orderService->fillFromRequest($request, $order);

        return redirect(route('admin.orders.index'))->with('success', trans('order_updated_successfully'));
    }

    public function products(Order $order)
    {
        $list = $order->products;

        return View::make('admin.orders.products.index', ['list' => $list,'order'=>$order]);
    }

    public function addProduct(Order $order)
    {
        $branchProducts = BranchProduct::all();

        return View::make('admin.orders.products.new', ['order' => $order, 'branchProducts' => $branchProducts]);
    }

    public function storeProduct(OrderProductsRequest $request, Order $order)
    {
        $this->orderService->fillOrderProductsFromRequest($request);

        return redirect(route('admin.order.products', ['order' => $order->id]))->with('success', trans('order_product_added_successfully'));
    }

    public function editProduct(OrderProduct $orderProduct)
    {
        $branchProducts = BranchProduct::all();

        return View::make('admin.orders.products.edit', ['orderProduct' => $orderProduct,'branchProducts'=>$branchProducts]);
    }

    public function updateProduct(OrderProductsRequest $request, OrderProduct $orderProduct)
    {
        $this->orderService->fillOrderProductsFromRequest($request, $orderProduct);

        return redirect(route('admin.order.products', ['order' => $orderProduct->order_id]))->with('success', trans('order_product_updated_successfully'));
    }

    public function destroyProduct(OrderProduct $orderProduct)
    {
        $orderProduct->delete();

        $this->orderService->setOrderTotalCost($orderProduct);

        return redirect()->back()->with('success', trans('order_product_deleted_successfully'));
    }

    public function export()
    {
        return $this->orderService->export();
    }
}
