<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller ;
use App\Constants\BranchTypes;
use App\Http\Requests\Admin\ProductRequest;
use App\Http\Services\ProductService;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\Unit;
use App\Models\Category;
use App\Repositories\ProductRepository;
use App\Repositories\BranchRepository;
use View;

class ProductController extends Controller
{

    private $productRepository;
    private $branchRepository;

    public function __construct(ProductService $productService, ProductRepository $productRepository
                                , BranchRepository $branchRepository)
    {
        $this->productService = $productService;
        $this->productRepository = $productRepository;
        $this->branchRepository = $branchRepository;
    }

    public function index()
    {
        $list = $this->productRepository->search(request())->paginate(10);
        $list->appends(request()->all());
        $count = $this->productRepository->search(request())->count();

        return View::make("admin.products.index", compact("list", "count"));
    }

    public function create()
    {
        return View::make("admin.products.create", compact("branchs"));
    }

    public function store(ProductRequest $request)
    {
        $product = $this->productService->fillFromRequest($request);
        return redirect(route("admin.products.index"))->with('success', trans('item_added_successfully'));
    }

    public function edit(Product $product)
    {
        return View::make("admin.products.edit", compact("product", 'units', 'categories'));
    }

    public function update(ProductRequest $request, Product $product)
    {
        $product = $this->productService->fillFromRequest($request, $product);
        return redirect(route("admin.products.index"))->with('success', trans('item_updated_successfully'));
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return back()->with('success', trans('item_deleted_successfully'));
    }

    public function deleteImage(Product $product, ProductImage $productImage)
    {
        $result = $this->productService->deleteImage($product, $productImage);

        if ($result) {
            return back()->with('success', trans('item_deleted_successfully'));
        }

        return back()->with('error', trans('error_not_complete'));
    }

}
