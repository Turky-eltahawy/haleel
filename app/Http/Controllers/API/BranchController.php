<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Branch;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    public function nearest(Request $request)
    {
        if ($request->has('lat') && $request->has('lng')) {
            $branches =   Branch::orderByDistance('location', new Point($request->get('lat'), $request->get('lng')))->get();
            return response()->json([
                'message' => 'data retrived',
                'data' => [
                    'branches' => $branches
                ]
            ], 200);
        }
        return response()->json(
            [
                'message' => "invaid or missing data ...",
                'data' => []
            ],
            400
        );
    }
}
