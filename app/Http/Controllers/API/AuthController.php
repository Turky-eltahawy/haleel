<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\LoginRequest;
use App\Http\Requests\API\RegisterRequest;
use App\Models\FcmToken;
use App\Models\User;

class AuthController extends Controller
{


    public function register(RegisterRequest $register_request)
    {
        //create user 
        $user = User::create([
            'name' => $register_request->get('name'),
            'phone' => $register_request->get('phone')
        ]);
        //set fcm to user
        $user->fcm()->create(
            [
                'type' => $register_request->get('type'),
                'token' => $register_request->get('fcm_token')
            ]
        );
        //attempt user to login 
        $user->token = auth('api')->login($user);
        return response()->json([
            'message' => 'User Created ',
            'data' => [
                'user' => $user,
            ]
        ], 201);
    }
    /**
     * Login user 
     *
     * @param LoginRequest $loginRequest
     * @return token
     */
    public function login(LoginRequest $loginRequest)
    {
        $user = User::whereEmail($loginRequest->get('email'))->first();
        if (!$user) {
            return response()->json([
                'message' => "unauthorized",
                "data" => []
            ], 401);
        }

        $user->fcm()->update(
            [
                'type' => $loginRequest->get('type'),
                'token' => $loginRequest->get('fcm_token')
            ]
        );

        $user->token =  auth('api')->login($user);
        return response()->json([
            'message' => 'logged in successfully',
            'data' => [
                'user' => $user
            ]
        ]);
    }
    /**
     * logout user  
     *
     * @return void
     */
    public function logout()
    {
        auth('api')->logout();
        return response()->json(
            [
                'message' => "logout",
                'data' => []
            ],
            200
        );
    }
    /**
     * Refresh token 
     *
     * @return token
     */
    public function refresh()
    {
        return  response()->json([
            'message' => 'token refreshed',
            'data' => [
                'token' => auth('api')->refresh()
            ]

        ]);
    }
    /**
     * retrive user
     *
     * @return User
     */
    public function me()
    {
        return response()->json([
            'message' => 'user retrived',
            'data' => [
                'user' => auth('api')->user()
            ]
        ]);
    }
}
