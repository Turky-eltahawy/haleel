<?php

namespace App\Http\Resources;

use App\Constants\OrderStatus;
use Illuminate\Http\Resources\Json\Resource;

class Orders extends Resource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user->name,
            'address_id' => $this->address_id ? $this->address->name : '-',
            'branch_id' => $this->branch_id ? $this->branch->name : '-',
            'total_price' => $this->total_price,
            'created_at' => $this->created_at,
            'status' => OrderStatus::getValue($this->status)
        ];
    }
}
$inputs=
[
    'ahmed' => ['pass' => '', 'products' => ['xx', 'yy']],
    'mohamed' => []
];