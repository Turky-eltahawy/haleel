<?php

namespace App\Repositories;

use App\Models\Branch;
use App\Models\BranchProduct;
use Symfony\Component\HttpFoundation\Request;

class BranchRepository
{

    /**
     * @param $request
     * @return $this|mixed
     */
    public function search()
    {
        $branches = Branch::query()->orderByDesc("id");

        if (request('filter_by') == "vendor_id" && !empty(request('q'))) {
            $branches->where($request->get('filter_by'), $request->get('q'));
        }

        return $branches;
    }
}
