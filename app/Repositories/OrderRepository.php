<?php

namespace App\Repositories;

use App\Constants\OrderStatus;
use App\Constants\UserTypes;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderRepository
{
    public function search()
    {
        $orders = Order::query()->orderByDesc("id");
        if (request('banch_id') ) {
            $orders->where(request('banch_id'), request('q'));
        }
        if (request()->filled('status')) {
            $orders->where('status', request('status'));
        }

        if (request()->filled('from_date')) {
            $orders->where('created_at', '>=', request('from_date'));
        }
        if (request()->filled('to_date') ) {
            $orders->where('created_at', '<=', request('to_date'));
        }

        return $orders;
    }

    public function products($order)
    {
        $products = Order::with(['products','products.product','products.product.images', 'address', 'user'])
            ->where('id', $order->id)
            ->orderBy("created_at", "DESC");

        return $products;
    }


    public function checkPromotionCodeExists($code)
    {
        if ($promotion = Voucher::where('code', $code)->where('is_used', 0)->where('active', 1)->first()) {
            return $promotion;
        }
        if ($promotion = Coupon::where('code', $code)->where('is_used', 0)->where('active', 1)->first()) {
            return $promotion;
        }
        return false;
    }
}
