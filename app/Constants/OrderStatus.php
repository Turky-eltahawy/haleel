<?php

namespace App\Constants;

final class OrderStatus
{
    const SUBMITTED = 1;
    const ON_FIRE = 2;
    const PENDING = 3;
    const RECEIVED = 4;
    const CANCELLED = 5;

    public static function getList()
    {
        return [
            OrderStatus::SUBMITTED    => trans("submitted"),
            OrderStatus::ON_FIRE    => trans("on_fire"),
            OrderStatus::PENDING => trans("pending"),
            OrderStatus::RECEIVED   => trans("received"),
            OrderStatus::CANCELLED    => trans("cancelled"),
        ];
    }

    public static function getValue($key)
    {
        $list = self::getList();

        return isset($list[$key]) ? $list[$key] : '';
    }
}
