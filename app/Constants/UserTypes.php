<?php

namespace App\Constants;

final class UserTypes
{
    const CLIENT = 1;
    const ADMIN  = 2;

    public static function getList()
    {
        return [
            UserTypes::CLIENT => trans("client"),
            UserTypes::ADMIN => trans("admin"),
        ];
    }
}
