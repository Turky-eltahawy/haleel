<?php

namespace App\Constants;

final class BranchTypes
{

    const SMALL = 1 ;
    const BIG   = 2 ;

    public static function getBranchType()
    {
        return [
            BranchTypes::SMALL =>trans("small") ,
            BranchTypes::BIG => trans("big"),
        ];
    }
    public static function getTypeValue()
    {
        $values=[];
        foreach (BranchTypes::getBranchType() as $key => $value) {
            $values[]=$key;
        }
        return $values;
    }

}

