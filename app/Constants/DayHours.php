<?php

namespace App\Constants;

final class DayHours
{

    public static function getList($slug)
    {
        $AmHours = [];
        $PmHours = [];
        $i = 1 ;
        for($i ; $i<=12 ;$i++){
            if($slug == 'AM'){
                array_push($AmHours, $i.$slug);
            }
            else{
                array_push($PmHours , ($i+12).$slug);   
            }
        }
        return $slug == 'AM' ? $AmHours : $PmHours ;
    }

    public static function days()
    {
        return [
            WeekDays::SATURDAY =>"saturday",
            WeekDays::SUNDAY =>"sunday",
            WeekDays::MONDAY =>"monday",
            WeekDays::TUESDAY =>"tuesday",
            WeekDays::WEDNESDAY =>"wednesday",
            WeekDays::THURSDAY =>"thursday",
            WeekDays::FRIDAY =>"friday",
        ];
    }

    public static function getDay($key)
    {
        return self::days()[$key];
    }

    public static function getOne($key)
    {
        return self::getList()[$key];
    }
}
