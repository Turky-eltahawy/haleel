<?php

namespace App\Constants;

final class ProductTypes
{

    const SANDWICH = 1 ;
    const BOX   = 2 ;
    const EXTRA   = 3 ;

    public static function getList()
    {
        return [
            ProductTypes::SANDWICH =>trans("sandwich") ,
            ProductTypes::BOX => trans("box"),
            ProductTypes::EXTRA => trans("extra"),
        ];
    }
}
