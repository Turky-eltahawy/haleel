<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FcmToken extends Model
{

    protected $fillable = ['type', 'token'];
    const TYPE = ['Ios' => 1, 'Android' => 2];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
