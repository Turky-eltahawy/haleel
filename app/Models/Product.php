<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;

class Product extends Model implements TranslatableContract
{
    use Translatable ;
    public $translatedAttributes = ['name', 'description'];
    protected $hidden = ['translations'];
    protected $appends = array('name', 'description');


}
