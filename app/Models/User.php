<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $fillable = [
        'name', 'type', 'email', 'password', 'address', 'phone'
    ];
    public $Attributes = [
        'notification ' => 1
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = \Hash::make($pass);
    }

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    /**
     * Set Relation with fcm token
     *
     * @return HasOne
     */
    public function fcm()
    {

        return $this->hasOne(FcmToken::class);
    }
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
