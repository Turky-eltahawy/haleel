<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Spatie\Translatable\HasTranslations;

class Branch extends Model
{
    use SpatialTrait, HasTranslations;

    /**
     * Geo point 
     */
    protected $spatialFields = [
        'location'
    ];
    /**
     * translatable field 
     */
    public $translatable = ['address'];

    protected $fillable = ['location', 'address'];
}
