<?php

namespace App\Policies;

use App\Constants\UserTypes;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        // check if the user is not admin return false before access any rolls
        if ($user->isTypeOf(UserTypes::ADMIN) && !$user->isTypeOf(UserTypes::VENDOR)) {
            return false;
        }
    }


    public function index(User $user)
    {
        return $user->type == UserTypes::ADMIN ;
    }

    public function create(User $user)
    {
        return $user->type == UserTypes::ADMIN ;

    }

    public function update(User $user, User $model)
    {
        return $user->type == UserTypes::ADMIN ;
    }

    public function delete(User $user, User $model)
    {
        return $user->type == UserTypes::ADMIN ;
    }

}
